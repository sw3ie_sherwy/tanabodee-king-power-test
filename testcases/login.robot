*** Settings ***
Resource        ${CURDIR}/../keywords/login_keywords.robot

Test Setup         Open Login Page
Test Teardown      Close All Browsers

*** Test Cases ***
TC_00001 - Login Success
    [Documentation]    To verify that users can login successfully when put a correct username and password
    Login to System         ${existing_user.username}      ${existing_user.password}
    Verify Success Flash Message        You logged into a secure area!
    Logout
    Verify Success Flash Message        You logged out of the secure area!

TC_00002 - Login failed - Password incorrect
    [Documentation]    To verify that users can login unsuccessfully when they put a correct username but wrong password
    Login to System         ${existing_user.username}      Password!​
    Verify Error Flash Message        Your password is invalid!

TC_00003 - Login failed - Username not found
    [Documentation]    To verify that users can login unsuccessfully when they put a username that did not exist
    Login to System         tomholland      Password!​
    Verify Error Flash Message        Your username is invalid!