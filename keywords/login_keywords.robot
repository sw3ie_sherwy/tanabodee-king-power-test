*** Settings ***
Resource        ${CURDIR}/../resources/imports.robot
Variables       ${CURDIR}/../resources/page_resources.yaml


*** Keywords ***
Open Login Page
    ${url}        Set Variable       ${LOGIN_HOST}${LOGIN_URL}
    Open Browser        ${LOGIN_HOST}${LOGIN_URL}      chrome

Login to System
    [Arguments]     ${username}     ${password}
    Input Text      ${LOGIN_USERNAME_TXT}         ${username}
    Input Text      ${LOGIN_PASSWORD_TXT}         ${password}
    Click Element           ${LOGIN_BTN}

Logout
    Click Element           ${LOGOUT_BTN}

Verify Success Flash Message
    [Arguments]     ${expected_msg}
    ${actual_msg}       Get Text        ${FLASH_MESSAGE_SUCCESS_ELE}
    ${actual_msg}       Strip String        ${actual_msg}
    Should Contain      ${actual_msg}       ${expected_msg}

Verify Error Flash Message
    [Arguments]     ${expected_msg}
    ${actual_msg}       Get Text        ${FLASH_MESSAGE_ERROR_ELE}
    ${actual_msg}       Strip String        ${actual_msg}
    Should Contain      ${actual_msg}       ${expected_msg}